<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
// use Excel;
use Maatwebsite\Excel\Facades\Excel;

class ExportExcelController extends Controller
{
     function index3()
    {
     $data = DB::table('dekripsis')->get();
     return view('export_excel')->with('data', $data);

    }

 function excel()
    {
        $data = DB::table('dekripsis')->get()->toArray();
        $data_array[] = array('Date of loss', 'Location', 'Location Details', 'Flight Number', 'Origin','Destination','Type of Property','Item Characteristics','Picture of Item','Name','Email','Phone Number');
        foreach($data as $p)
        {
         $data_array[] = array(
          'Date of loss'            => $p->date,
          'Location'                => $p->location,
          'Location Details'        => $p->locationdetails,
          'Flight Number'           => $p->flightnumber,
          'Origin'                  => $p->origin,
          'Destination'             => $p->destination,
          'Type of Property'        => $p->typeproperty,
          'Item Characteristics'    => $p->other,
          'Picture of Item'         => $p->itemimage,
          'Name'                    => $p->name,
          'Email'                   => $p->email,
          'Phone Number'            => $p->phone

         );
        }
        Excel::create('dekripsis', function($excel) use ($data_array){
            $excel->setTitle('dekripsis');
            $excel->sheet('dekripsis', function($sheet) use ($data_array){
             $sheet->fromArray($data_array, null, 'A1', false, false);
            });
           })->download('xlsx');
          }
      
   
}


