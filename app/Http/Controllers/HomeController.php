<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use App\Dekripsi;
use App\City;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    public function index2()
    {
        $city = City::all();
        return view('generate',['city'=> $city]);
    }

    public function filter() {
        $data = ['date', 'origin', 'destination'];
        $r = ['from', 'to'];
        $collection = [];
        $range = [];

        // get data, data apa gatau wkwk
        for ($i=0; $i < count($data); $i++) { 
            if(!empty(request($data[$i]))){
                $collection[$data[$i]] = request($data[$i]);
            }
        }

        // get from sama to
        for ($i=0; $i < count($r); $i++) { 
            if(!empty(request($r[$i]))){
                $range[$r[$i]] = request($r[$i]);
            }
        }

        $city = City::all();

        // merubah array ke array object
        if(count($range) > 0) {
            $new_col = collect([
                (object)$collection
            ]);

            $range_col = collect([
                (object)$range
            ]);

            $filter = DB::table('dekripsis')->where($collection)
                    ->whereBetween('date', [$range['from'], $range['to']])->get();
            
            return view('generate', ['filter' => $filter, 'city'=> $city, 'collection' => $new_col, 'range' => $range_col]);
        }
        else{
            $new_col = collect([
                (object)$collection
            ]);

            $filter = DB::table('dekripsis')->where($collection)->get();

            return view('generate', ['filter' => $filter, 'city'=> $city, 'collection' => $new_col, 'range' => '']);
        }
        

        
    }

    public function report(){
        return view('genreport');
    }
    
    public function showFunction(){
        $dekripsi = DB::table('dekripsi')->get();
        return view('');
    }

    function excel($from=NULL, $to=NULL, $origin=NULL, $destination=NULL)
    {
        $collection = [];
        $date = [];
        $range = [];

        if($from != NULL){
            $range['from'] = $from;
        }

        if($to != NULL){
            $range['to'] = $to;
        }

        // if($flight_date != NULL){
        //     $date['date'] = $flight_date;
        // }

        if($origin != NULL){
            $collection['origin'] = $origin;
        }

        if($destination != NULL){
            $collection['destination'] = $destination;
        }

        if($from != NULL) {
            $data =  DB::table('dekripsis')->where($collection)
                        ->whereBetween('date', [$range['from'], $range['to']])->get()->toArray();
        }
        else{
            $data =  DB::table('dekripsis')->where($collection)->where($date)->get()->toArray();
        }
        
        $data_array[] = array('Date of loss', 'Location', 'Location Details', 'Flight Number', 'Origin','Destination','Type of Property','Item Characteristics','Picture of Item','Name','Email','Phone Number');
        foreach($data as $p)
        {
         $data_array[] = array(
          'Date of loss'            => $p->date,
          'Location'                => $p->location,
          'Location Details'        => $p->locationdetails,
          'Flight Number'           => $p->flightnumber,
          'Origin'                  => $p->origin,
          'Destination'             => $p->destination,
          'Type of Property'        => $p->typeproperty,
          'Item Characteristics'    => $p->other,
          'Picture of Item'         => $p->itemimage,
          'Name'                    => $p->name,
          'Email'                   => $p->email,
          'Phone Number'            => $p->phone

         );
        }
        Excel::create('dekripsis', function($excel) use ($data_array){
            $excel->setTitle('dekripsis');
            $excel->sheet('dekripsis', function($sheet) use ($data_array){
             $sheet->fromArray($data_array, null, 'A1', false, false);
            });
        })->download('xlsx');
    }
    


    
}
