<?php

namespace App\Http\Controllers;
// gunakan model untuk table dekripsi
use App\Dekripsi;
use App\City;
use Carbon\Carbon;
use App\Mail\FormEmail;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;


class DekripsiController extends Controller
{
    public function infFunction(){
        return view('information');
    }


    public function descFunction() {
        $city = City::all();
        return view('description',['city'=>$city]);
    }

    public function admFunction(){
        return view('admin');
    }

    public function storeDesc() {
        $gambar = request()->file('itemimage');
        $nama = time().'.'.$gambar->getClientOriginalExtension();
        $path = public_path('/image');
        $gambar->move($path, $nama);

        $data = Dekripsi::create([
            'date' => Carbon::parse(request('date')),
            'location' => request('location'),
            'locationdetails' => request('locationdetails'),
            'flightnumber' => request('flightnumber'),
            'origin' => request('origin'),
            'destination' => request('destination'),
            'typeproperty' => request('typeofproperty'),
            'other' => request('other'),
            'itemimage' => $nama,
            'name' => request('name'),
            'email' => request('email'),
            'phone' => request('phone'),
            ]);
        
        // Mail::send(['email.lost_found_email','email.lost_found_email'], ["data" => $data], function ($message) use ($data) {
        //     $message->subject("Confirmation");
        //     $message->from('lost-found@garuda-indonesia.com','Lost & Found');
        //     $message->to($data["email"]);
        // });


    
        $this->send_email($data["email"], $data);

        return view('confirmation', ['data' => $data]);
    }
  

    // public function index(){
 
    //     Mail::to("ellvinareksi@gmail.com")->send(new FormEmail());
 
    //     return "Email telah dikirim";
 
    // }
    
    function send_email($email,$data) {
        Mail::send(['email.lost_found_email','email.lost_found_email'], ["data" => $data], function ($message) use ($email) {
            $message->subject("Confirmation");
            $message->from('lost-found@garuda-indonesia.com','Lost & Found');
            $message->to($email);
        });
  
    }
}
