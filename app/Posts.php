<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Posts extends Model
{
    protected $fillable = ['date', 'location','locationdetails','flightnumber','origin','destination','typeproperty','other','itemimage','name','email','phone'];
}
