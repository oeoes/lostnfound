<div class="container">
  <div class="jumbotron"> 
    <h1 align="center">Lost Property</h1>      
    <p>We're sorry to hear that you lost something, but we'll do our best to check and restore it to you.</p>
    <p>We will ask you to describe the item, including where and when you lost it. That way, it will increase the chance of us finding it for you.</p>
    <p>Enquiries regarding items that has been lost for more than 90 days since the lost date will not be followed up.</p>  
     <ul class="pager">
    <li class="previous"><a href="#">Cancel</a></li>
    <li class="next"><a href="#">Next</a></li>
  </ul> 
  </div>   
</div>