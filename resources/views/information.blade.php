@extends('layouts.master')
@section('article')

<div class="text-center">
<ul class="pagination">
  <li class="active"><a href="/">Information</a></li>
  <li><a href="description">Description</a></li>
  <li><a href="personal">Personal Details</a></li>
  <li ><a href="confirmation">Confirmation</a></li>
</ul>
</div>

<!-- Form Input property -->
<form method="post" action="{{ route('store.desc') }}" enctype="multipart/form-data">
    {{ csrf_field() }}


<div class="container">
  <div class="jumbotron"> 
    <h1 align="center">Lost Property</h1>      
    <p>We're sorry to hear that you lost something, but we'll do our best to check and restore it to you.</p>
    <p>We will ask you to describe the item, including where and when you lost it. That way, it will increase the chance of us finding it for you.</p>
    <p>Enquiries regarding items that has been lost for more than 90 days since the lost date will not be followed up.</p>  
     <ul class="pager">
     <li class="previous"><a href="/information">Cancel</a></li>
    <li class="next"><a href="desc">Next</a></li>
  </ul> 
  </div>   
</div>

</form>
@endsection

