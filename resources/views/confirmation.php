@extends('layouts.master')
@section('article')

<div class="text-center">
<ul class="pagination">
  <li ><a href="/">Information</a></li>
  <li><a href="description">Description</a></li>
  <li><a href="personal">Personal Details</a></li>
  <li class="active"><a href="confirmation">Confirmation</a></li>
</ul>
</div>


<!-- Form Input property -->
<form method="post" action="{{ route('store.desc') }}" enctype="multipart/form-data">
    {{ csrf_field() }}


    <div class="container">
  <div class="jumbotron"> 
  <h2 align="center"> Summary </h2>  
  <br>
  <h3 align="center">Here is summary of your lost report </h3> 
  <p> Thank you for reporting about your lost property We will try our best to keep you informed, please wait up to 3 days</p>
  <p> Our team will keep you updated if there is any further information regarding your lost property </p>

  <table class="table">
    <thead>
    <tr>
        <th>Name</th>
        <td>{{ $data->name }}</td>
      </tr>

      <tr>
        <th>Email</th>
        <td>{{ $data->email }}</td>
      </tr>

      <tr>
        <th>Phone Number</th>
        <td>{{ $data->phone }}</td>
      </tr>

      <tr>
        <th>Location</th>
        <td>{{ $data->location }}</td>
      </tr>
      <tr>
        <th>Location Details</th>
        <td>{{ $data->locationdetails }}</td>
      </tr>
      <tr>
        <th>Origin</th>
        <td>{{ $data->origin }}</td>
      </tr>
      <tr>
        <th>Destination</th>
        <td>{{ $data->destination }}</td>
      </tr>
      <tr>
        <th>Property</th>
        <td>{{ $data->typeproperty }}</td>
      </tr>
      <tr>
        <th>Others</th>
        <td>{{ $data->other }}</td>
      </tr>
      <tr>
        <th>Image</th>
        <td>{{ $data->itemimage }}</td>
      </tr>
    </thead>
  </table>
  

     <ul class="pager">
    <li class="previous"><a href="personal">Cancel</a></li>
    <li class="btn btn-primary">Submit</button></li>
    </ul> 

  </div>   
</div>

@endsection