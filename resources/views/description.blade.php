@extends('layouts.master')

@section('article')

<!-- Form Input property -->
<form method="post" action="{{ route('store.desc') }}" enctype="multipart/form-data">
    {{ csrf_field() }}

<!-- Form Pertama -->

<div class="text-center">
<ul class="pagination">
  <li ><a href="/">Information</a></li>
  <li class="active"><a href="description">Description</a></li>
  <li><a href="personal">Personal Details</a></li>
  <li><a href="confirmation">Confirmation</a></li>
</ul>
</div>

<div class="jumbotron" id="jumb1">   
    <h2 align="center">Describe your property</h2>
    <br>
  
        <div class="form-group row">
            <label for="dateid" class="col-sm-3 col-form-label">Date of loss</label>
            <div class="col-sm-9">
            <input name="date" type="text" class="form-control" id="datepicker">
            </div>
        </div>

        <div class="form-group row">
            <label for="location" class="col-sm-3 col-form-label">Location</label>
            <div class="col-sm-9" >
            <select name="location" class="form-control" id="sel1">
                <option value="flight">Flight</option>
                <option value="airport">Airport</option>
            </select>
            </div>
        </div>


        <div class="form-group row">
            <label for="locationdetails" class="col-sm-3 col-form-label">Location Details</label>
            <div class="col-sm-9">
                <input name="locationdetails" type="text" class="form-control" id="locationdetailsid"
                       placeholder="Location Details">
            </div>
        </div>

        <div class="form-group row">
            <label for="flightnumber" class="col-sm-3 col-form-label">Flight Number</label>
            <div class="col-sm-9">
                <input name="flightnumber" type="text" class="form-control" id="flightnumberid"
                       placeholder="Flight Number">
            </div>
        </div>

      

            <div class="form-group row">
            <label for="origin" class="col-sm-3 col-form-label">Origin</label>
            <div class="col-sm-9" >
            <select name="origin" class="form-control" id="">
            <option disabled selected value> -- select an option -- </option>
            @foreach($city as $c)
                    <option value="{{ $c->city }}">{{ $c->city }}</option>
                @endforeach
            </select>
            </div>
        </div>

        <div class="form-group row">
            <label for="destination" class="col-sm-3 col-form-label">Destination</label>
            <div class="col-sm-9" >
            <select name="destination" class="form-control" id="">
            <option disabled selected value> -- select an option -- </option>
            @foreach($city as $c)
                        <option value="{{ $c->city }}">{{ $c->city }}</option>
                    @endforeach
            </select>
            </div>
        </div>
        <div class="form-group row">
            <label for="typeproperty" class="col-sm-3 col-form-label">Type of Property</label>
            <div class="col-sm-9" >
            <div class="form-group">
                    <select name="typeofproperty" class="form-control" id="sel1">
                        <option value="agenda">Agenda</option>
                        <option value="artwork">Artwork</option>
                        <option value="audio">Audio</option>
                        <option value="book">Book</option>
                        <option value="clothing">Clothing</option>
                        <option value="computer">Computer</option>
                        <option value="currency">Currency</option>
                        <option value="document">Document</option>
                        <option value="dutyfree">Duty Free</option>
                        <option value="glasses">Glasses</option>
                        <option value="id">ID</option>
                        <option value="jewellery">Jewellery</option>
                        <option value="key">Key</option>
                        <option value="luggage">Luggage</option>
                        <option value="medication">Medication</option>
                        <option value="musical">Musical Instrument</option>
                        <option value="phone">Phone</option>
                        <option value="toys">Toys</option>
                        <option value="video">Video</option>
                        <option value="wallet">Wallet</option>
                        <option value="other">Other</option>
                    </select>
                    </div>
            </div>
        </div>

        <div class="form-group row">
            <label for="other" class="col-sm-3 col-form-label">Item Characteristics</label>
            <div class="col-sm-9">
                <input name="other" type="text" class="form-control" id="otherid"
                       placeholder="Please write down the item characteristics">
            </div>
        </div>
    
        <div class="form-group row">
            <label for="itemimage" class="col-sm-3 col-form-label">Picture of Item</label>
            <div class="col-sm-9">
                <input name="itemimage" type="file" id="itemimageid" class="custom-file-input">
                <span style="margin-left: 15px; width: 480px;" class="custom-file-control"></span>
            </div>
        </div>
    

     <ul class="pager">
    <li class="previous"><a href="/information">Cancel</a></li>
    <li class="next"><a id="next-form" href="">Next</a></li>
  </ul>   
</div>


<!-- Form kedua -->


<div class="jumbotron" id="jumb2" style="display: none">   
    <h2 align="center">Personal Information</h2>

        <div class="form-group row">
            <label for="name" class="col-sm-3 col-form-label">Name</label>
            <div class="col-sm-9">
                <input name="name" type="text" class="form-control" id="nameid"
                       placeholder="Name">
            </div>
        </div>

        <div class="form-group row">
            <label for="emailid" class="col-sm-3 col-form-label">Email</label>
            <div class="col-sm-9">
                <input name="email" type="text" class="form-control" id="emailid"
                       placeholder="Email">
            </div>
        </div>

        <div class="form-group row">
            <label for="phoneid" class="col-sm-3 col-form-label">Phone Number</label>
            <div class="col-sm-9">
                <input name="phone" type="text" class="form-control" id="phoneid"
                       placeholder="Phone Number">
            </div>
        </div>

     <ul class="pager">
    <li class="next"><input type="submit" value="Next"></li>
  </ul> 

</div>
</form>


@endsection