<!DOCTYPE html>
<html>
 <head>
  <title>Export Data to Excel in Laravel using Maatwebsite</title>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <style type="text/css">
   .box{
    width:600px;
    margin:0 auto;
    border:1px solid #ccc;
   }
  </style>
 </head>
 <body>
  <br />
  <div class="container">
   <h3 align="center">Export Data to Excel in Laravel using Maatwebsite</h3><br />
   <div align="center">
    <a href="{{ route('export_excel.excel') }}" class="btn btn-success">Export to Excel</a>
   </div>
   <br />
 <div class="table-responsive">
    <table class="table table-striped table-bordered">
     <tr>
      <td>Date of loss</td>
      <td>Location</td>
      <td>Location Details</td>
      <td>Flight Number</td>
      <td>Origin</td>
      <td>Destination</td>
      <td>Type of Property</td>
      <td>Item Characteristics</td>
      <td>Picture of Item</td>
      <td>Nama</td>
      <td>Email</td>
      <td>Phone Number</td>
     </tr>

     @foreach($data as $d)
     <tr>
                        <td>{{ $d->date }}</td>
                        <td>{{ $d->location }}</td>
                        <td>{{ $d->locationdetails }}</td>
                        <td>{{ $d->flightnumber }}</td>
                        <td>{{ $d->origin }}</td>
                        <td>{{ $d->destination }}</td>
                        <td>{{ $d->typeproperty }}</td>
                        <td>{{ $d->other }}</td>
                        <td><img src="{{ asset('image/').'/'.$d->itemimage }}" alt="" style="width: 200px"></td>
                        <td>{{ $d->name }}</td>
                        <td>{{ $d->email }}</td>
                        <td>{{ $d->phone }}</td>
     </tr>
     @endforeach
    </table>
   </div>
   
  </div>
 </body>
</html>


