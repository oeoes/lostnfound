<!DOCTYPE html>
<html lang="en">
<head>
  <title>Lost Property</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
</head>
<body>

<div class="text-center">
<ul class="pagination">
  <li ><a href="/">Information</a></li>
  <li ><a href="description">Description</a></li>
  <li class="active"><a href="personal">Personal Details</a></li>
  <li><a href="confirmation">Confirmation</a></li>
</ul>
</div>

<div class="container">
  <div class="jumbotron">   
  <h2 align="center">Please fill in your contact</h2>
  <br>
    <form method="post" action="/description" enctype="multipart/form-data">


        <div class="form-group row">
            <label for="nameid" class="col-sm-3 col-form-label">Name</label>
            <div class="col-sm-9">
                <input name="name" type="text" class="form-control" id="nameid"
                       placeholder="Name">
            </div>
        </div>

        <div class="form-group row">
            <label for="emailid" class="col-sm-3 col-form-label">Email</label>
            <div class="col-sm-9">
                <input name="email" type="text" class="form-control" id="emailid"
                       placeholder="Email">
            </div>
        </div>

        <div class="form-group row">
            <label for="phoneid" class="col-sm-3 col-form-label">Phone Number</label>
            <div class="col-sm-9">
                <input name="phone" type="text" class="form-control" id="phoneid"
                       placeholder="Phone Number">
            </div>
        </div>

        
    </form>

     <ul class="pager">
    <li class="previous"><a href="description">Cancel</a></li>
    <li class="next"><a href="confirmation">Next</a></li>
  </ul> 
  </div>   
</div>

</body>
</html>
