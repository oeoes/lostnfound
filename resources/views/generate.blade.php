@extends('layouts.app')

@section('content')



<div class="container-fluid">
  <div class="row content">
    <div class="col-sm-3 sidenav">
      <h4>Garuda Indonesia</h4>
      <ul class="nav nav-pills nav-stacked">
      <ul class="nav nav-pills nav-stacked">
        <li><a href="home">Home</a></li>
        <li class="active"><a href="generate">Generate Report</a></li>
        <li><a href="#section3">Menu</a></li>
        <li><a href="#section3">Lainnya</a></li>
      </ul><br>
      <div class="input-group">
        <input type="text" class="form-control" placeholder="Search Blog..">
        <span class="input-group-btn">
          <button class="btn btn-default" type="button">
            <span class="glyphicon glyphicon-search"></span>
          </button>
        </span>
      </div>
    </div>
    <div class="col-sm-9">
    <div id="page-wrapper">
  <div class="row">
    <div class="col-lg-12">
      <h1>Generate Report <small>Lost & Found</small></h1>
      <ol class="breadcrumb">
        <li class="active"><i class="fa fa-dashboard"></i> Generate Report</li>
      </ol>
      <table width="900">
        <tr>
          <td width="250"><div class="Tanggal"><h4><script language="JavaScript">document.write(tanggallengkap);</script></div></h4></td> 
          <td align="left" width="30"> - </td>
          <td align="left" width="620"> <h4><div id="output" class="jam" ></div></h4></td>
        </tr>
      </table>
      <br />
      <div class="alert alert-success alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        Selamat Datang Di Halaman Admin Lost & Found Garuda Indonesia.. 
      </div>

      <div class="col-lg-12">
          <div class="panel panel-primary">
            <div class="panel-heading">
              <h3 class="panel-title"><i class="fa fa-user"></i> Generate Report </h3> 
            </div>
            <div class="panel-body">

            <!-- Filter -->
              <form class="form-horizontal" action="{{ route('filter') }}" method="POST">
              {{ csrf_field() }}
                <div class="form-group range">
                  <label class="col-sm-2 control-label">From</label>
                  <div class="col-sm-3">
                    <input name="from" type="date" class="form-control" placeholder="from">
                  </div>
                  <label class="col-sm-2 control-label">To</label>
                  <div class="col-sm-3">
                    <input name="to" type="date" class="form-control" placeholder="to">
                  </div>
                </div>
                
                <!-- <div class="form-group flightdate">
                  <label class="col-sm-2 control-label">Flight Date</label>
                  <div class="col-sm-9">
                    <input name="date" type="date" class="form-control" placeholder="Flight Date">
                  </div>
                </div> -->

                <div class="form-group">
                    <label class="col-sm-2 control-label">Origin</label>
                    <div class="col-sm-3" >
                    <select name="origin" class="form-control" id="" required>
                        <option disabled selected value> -- select an option -- </option>
                        @foreach($city as $c)
                            <option value="{{ $c->city }}">{{ $c->city }}</option>
                        @endforeach
                    </select>
                </div>

            <label class="col-sm-2 control-label">Destination</label>
            <div class="col-sm-3" >
                <select name="destination" class="form-control" id="" required>
                    <option disabled selected value> -- select an option -- </option>
                    @foreach($city as $c)
                        <option value="{{ $c->city }}">{{ $c->city }}</option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-2 control-label">GA -</label>
            <div class="col-sm-9">
            <input name="flightnumber" type="text" class="form-control" id="flightdate" placeholder="...">
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-default">Submit</button>
            </div>
        </div>
        
    </form> 

              <div class="table-responsive">
                <table class="table table-bordered">
                <tr>
                    <th>Date of loss<i class="fa fa-sort"></i></th>
                    <th>Location<i class="fa fa-sort"></i></th>
                    <th>Location Details<i class="fa fa-sort"></i></th>
                    <th>Flight Number<i class="fa fa-sort"></i></th>
                    <th>Origin<i class="fa fa-sort"></i></th>
                    <th>Destination<i class="fa fa-sort"></i></th>
                    <th>Type of Property<i class="fa fa-sort"></i></th>
                    <th>Item Characteristics<i class="fa fa-sort"></i></th>
                    <th>Picture of Item<i class="fa fa-sort"></i></th>
                    <th>Nama<i class="fa fa-sort"></i></th>
                    <th>Email<i class="fa fa-sort"></i></th>
                    <th>Phone Number<i class="fa fa-sort"></i></th>
                </tr>
                @if(!empty($filter) > 0)
                    @foreach($filter as $d)
                    <tr>
                        <td>{{ $d->date }}</td>
                        <td>{{ $d->location }}</td>
                        <td>{{ $d->locationdetails }}</td>
                        <td>{{ $d->flightnumber }}</td>
                        <td>{{ $d->origin }}</td>
                        <td>{{ $d->destination }}</td>
                        <td>{{ $d->typeproperty }}</td>
                        <td>{{ $d->other }}</td>
                        <td><img src="{{ asset('image/').'/'.$d->itemimage }}" alt="" style="width: 200px"></td>
                        <td>{{ $d->name }}</td>
                        <td>{{ $d->email }}</td>
                        <td>{{ $d->phone }}</td>

                    </tr>
                    @endforeach
                    @if(count($range) > 0)
                      @if(count($collection[0]->origin) > 0)
                        @if(count($collection[0]->destination) > 0)
                          <a href="{{ route('to_excel', ['from' => $range[0]->from, 'to' => $range[0]->to, 'origin' => $collection[0]->origin, 'destination' => $collection[0]->destination]) }}" class="btn btn-info">Export to Excel</a>
                        @else
                          <a href="{{ route('to_excel', ['from' => $range[0]->from, 'to' => $range[0]->to, 'origin' => $collection[0]->origin]) }}" class="btn btn-info">Export to Excel</a>
                        @endif
                      @else
                        <a href="{{ route('to_excel', ['from' => $range[0]->from, 'to' => $range[0]->to]) }}" class="btn btn-info">Export to Excel</a>
                      @endif
                    @else
                      <a href="{{ route('to_excel', ['date' => $collection[0]->date, 'origin' => $collection[0]->origin, 'destination' => $collection[0]->destination]) }}" class="btn btn-info">Export to Excel</a>
                    @endif
                    
                @else
                
                @endif
                
                </table>
                

                













@endsection
