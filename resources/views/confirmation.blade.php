@extends('layouts.master')
@section('article')


<div class="text-center">
<ul class="pagination">
  <li ><a href="/">Information</a></li>
  <li><a href="description">Description</a></li>
  <li><a href="personal">Personal Details</a></li>
  <li class="active"><a href="confirmation">Confirmation</a></li>
</ul>
</div>


<!-- Form Input property -->
<form method="post" action="{{ route('store.desc') }}" enctype="multipart/form-data">
    {{ csrf_field() }}


    <div class="container">
  <div class="jumbotron"> 
  <h2 align="center"> Summary </h2>  
  <br>
  <p> Thank you for reporting about your lost property. We will do our best to notify you, please wait up to 3 days </p>
  <p> Our team will keep you updated if there is more information about your lost property </p>

   <table style="width:100%" class="table table-striped table-bordered table-hover table-condensed">
  
  <tr>
    <th>Name:</th>
    <td>{{ $data->name }}</td>
  </tr>
  <tr>
    <th>Email:</th>
    <td>{{ $data->email }}</td>
  </tr>
  <tr>
    <th>Phone Number:</th>
    <td>{{ $data->phone }}</td>
  </tr>
  <tr>
    <th>Location:</th>
    <td>{{ $data->location }}</td>
  </tr>
  <tr>
    <th>Location Details:</th>
    <td>{{ $data->locationdetails }}</td>
  </tr>
  <tr>
    <th>Origin:</th>
    <td>{{ $data->origin }}</td>
  </tr>
  <tr>
    <th>Destination:</th>
    <td>{{ $data->destination }}</td>
  </tr>
  <tr>
    <th>Property:</th>
    <td>{{ $data->typeproperty }}</td>
  </tr>
  <tr>
    <th>Others:</th>
    <td>{{ $data->other }}</td>
  </tr>
  <tr>
    <th>Image:</th>
    <td>{{ $data->itemimage }}</td>
  </tr>
</table>
   <br>
   <br>

     <ul class="pager">
    <li class="btn"><a href="/inf" >Close</button></li>
    </ul> 

  </div>   
</div>

@endsection