<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        Schema::create('Posts', function (Blueprint $table) {
            $table->increments('id');
            $table->date('date');

            $table->string('location');

            $table->string('locationdetails');

            $table->string('flightnumber');

            $table->string('origin');

            $table->string('destination');

            $table->string('typeproperty');

            $table->string('other');

            $table->string('itemimage');

            $table->string('name');

            $table->string('email');

            $table->string('phone');

            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Posts');
    }
}
